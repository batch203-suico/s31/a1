/*
- What directive is used by Node.js in loading the modules it needs?

 Answer: let http = require("http");

- What Node.js module contains a method for server creation?

 Answer:  createServer() method

- What is the method of the http object responsible for creating a server using Node.js?

 Answer: http.createServer() method

- What method of the response object allows us to set status codes and content types?

 Answer: response.writeHead(200, {"Content-Type": "text/plain"});

- Where will console.log() output its contents when run in Node.js?

 Answer: After in server.listen() method

- What property of the request object contains the address's endpoint?
 Answer: HTTP request

*/